@extends('layout')

@section('content')
    <div class="pad-content">
        <div class="css-about">
            <div class="container">
                <div class="row justify-content-center">
                    <div class="col-md-10">
                        <div class="title mb20">Disclaimers</div>
                        <div class="bdy">
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                            <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Pharetra, proin auctor dolor tellus laoreet diam tristique. Nunc dictum velit diam a, bibendum eu, gravida est. Pellentesque nunc diam accumsan sapien adipiscing ullamcorper odio lobortis accumsan sapien adipiscing ullamcorper odio lobortis.</p><br/>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		
	});
</script>
@endsection