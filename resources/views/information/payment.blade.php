@extends('layout')

@section('content')
    <div class="pad-content">
        <div class="css-payment">
            <div class="container">
                <div class="title">Payment</div>
                <div class="bdy">
                    <p>Please choose one of the following payment methods</p>
                </div>
                <ul class="l-payment">
                    <li>
                        <a class="active" data-payment="paypal">PayPal</a>
                    </li>
                    <li>
                        <a data-payment="transfer">Telegraphic Transfer</a>
                    </li>
                    <li>
                        <a data-payment="credit">Credit Card</a>
                    </li>
                    <li>
                        <a data-payment="letter">Letter of Credit</a>
                    </li>
                </ul>
                <section class="box-content-payment" id="paypal" data-anchor="paypal">
                    <div class="t1">PayPal</div>
                    <div class="row">
                        <div class="col-md-4 col-lg-3 my-auto">
                            <div class="img"><img src="{{ asset('images/paypal.jpg') }}" alt="" title=""/></div>
                        </div>
                        <div class="col-md-8 col-lg-9 my-auto">
                            <div class="desc">
                                <p>Lorem interdum augue risus nisi. Arcu, amet egestas nunc id sed bibendum. Nulla tempor pharetra, eget sodales mi quam morbi volutpat lorem. Velit consequat hac euismod nunc. Cursus nisi, purus nunc, sed non vivamus donec lobortis ut.</p><br/>
                                <p>Justo, ut pretium sollicitudin platea viverra arcu a egestas. Nulla maecenas eget lorem platea arcu. Tellus, feugiat lobortis nullam commodo donec egestas urna, scelerisque. Eleifend amet ut vel id tempus pharetra quis nibh etiam.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="box-content-payment" id="transfer" data-anchor="transfer">
                    <div class="t1">Telegraphic Transfer</div>
                    <div class="row">
                        <div class="col-md-4 col-lg-3 my-auto">
                            <div class="img"><img src="{{ asset('images/transfer.jpg') }}" alt="" title=""/></div>
                        </div>
                        <div class="col-md-8 col-lg-9 my-auto">
                            <div class="desc">
                                <p>Lorem interdum augue risus nisi. Arcu, amet egestas nunc id sed bibendum. Nulla tempor pharetra, eget sodales mi quam morbi volutpat lorem. Velit consequat hac euismod nunc. Cursus nisi, purus nunc, sed non vivamus donec lobortis ut.</p><br/>
                                <p>Justo, ut pretium sollicitudin platea viverra arcu a egestas. Nulla maecenas eget lorem platea arcu. Tellus, feugiat lobortis nullam commodo donec egestas urna, scelerisque. Eleifend amet ut vel id tempus pharetra quis nibh etiam.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="box-content-payment" id="credit" data-anchor="credit">
                    <div class="t1">Credit Card</div>
                    <div class="row">
                        <div class="col-md-4 col-lg-3 my-auto">
                            <div class="img"><img src="{{ asset('images/credit.jpg') }}" alt="" title=""/></div>
                        </div>
                        <div class="col-md-8 col-lg-9 my-auto">
                            <div class="desc">
                                <p>Lorem interdum augue risus nisi. Arcu, amet egestas nunc id sed bibendum. Nulla tempor pharetra, eget sodales mi quam morbi volutpat lorem. Velit consequat hac euismod nunc. Cursus nisi, purus nunc, sed non vivamus donec lobortis ut.</p><br/>
                                <p>Justo, ut pretium sollicitudin platea viverra arcu a egestas. Nulla maecenas eget lorem platea arcu. Tellus, feugiat lobortis nullam commodo donec egestas urna, scelerisque. Eleifend amet ut vel id tempus pharetra quis nibh etiam.</p>
                            </div>
                        </div>
                    </div>
                </section>
                <section class="box-content-payment" id="letter" data-anchor="letter">
                    <div class="t1">Letter of Credit</div>
                    <div class="row">
                        <div class="col-md-4 col-lg-3 my-auto">
                            <div class="img"><img src="{{ asset('images/letter.jpg') }}" alt="" title=""/></div>
                        </div>
                        <div class="col-md-8 col-lg-9 my-auto">
                            <div class="desc">
                                <p>Lorem interdum augue risus nisi. Arcu, amet egestas nunc id sed bibendum. Nulla tempor pharetra, eget sodales mi quam morbi volutpat lorem. Velit consequat hac euismod nunc. Cursus nisi, purus nunc, sed non vivamus donec lobortis ut.</p><br/>
                                <p>Justo, ut pretium sollicitudin platea viverra arcu a egestas. Nulla maecenas eget lorem platea arcu. Tellus, feugiat lobortis nullam commodo donec egestas urna, scelerisque. Eleifend amet ut vel id tempus pharetra quis nibh etiam.</p>
                            </div>
                        </div>
                    </div>
                </section>
            </div>
        </div>
    </div>
@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-payment').addClass('active');

        $('ul.l-payment li a').click(function(event) {
            $('ul.l-payment li a').removeClass('active');
            $(this).addClass('active');
            var scrollAnchor = $(this).attr('data-payment'),
            scrollPoint = $('section[data-anchor="' + scrollAnchor + '"]').offset().top - 10;
            $('body,html').animate({ scrollTop: scrollPoint }, 500);
            return false;
        });
	});
</script>
@endsection