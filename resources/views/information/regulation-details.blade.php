@extends('layout')

@section('content')
    <div class="pad-content">
        <div class="css-regulation">
            <div class="container">
                <div class="title">Regulation Details</div>
                <div class="bdy">
                    <p>Regulation details for all our destination countries</p>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pos-rel">
                                <div class="img">
                                    <img src="{{ asset('images/japan.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="text">Japan</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Odio est tellus in turpis ligula rhoncus. Quam vitae suspendisse orci viverra eleifend semper. Viverra morbi sed enim eget aliquet. Eu sed lobortis lorem vulputate felis arcu felis fermentum. Eget massa praesent sed lorem gravida mattis auctor pretium nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pos-rel">
                                <div class="img">
                                    <img src="{{ asset('images/korea.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="text">Korea</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Odio est tellus in turpis ligula rhoncus. Quam vitae suspendisse orci viverra eleifend semper. Viverra morbi sed enim eget aliquet. Eu sed lobortis lorem vulputate felis arcu felis fermentum. Eget massa praesent sed lorem gravida mattis auctor pretium nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pos-rel">
                                <div class="img">
                                    <img src="{{ asset('images/japan.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="text">Japan</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Odio est tellus in turpis ligula rhoncus. Quam vitae suspendisse orci viverra eleifend semper. Viverra morbi sed enim eget aliquet. Eu sed lobortis lorem vulputate felis arcu felis fermentum. Eget massa praesent sed lorem gravida mattis auctor pretium nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pos-rel">
                                <div class="img">
                                    <img src="{{ asset('images/korea.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="text">Korea</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Odio est tellus in turpis ligula rhoncus. Quam vitae suspendisse orci viverra eleifend semper. Viverra morbi sed enim eget aliquet. Eu sed lobortis lorem vulputate felis arcu felis fermentum. Eget massa praesent sed lorem gravida mattis auctor pretium nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pos-rel">
                                <div class="img">
                                    <img src="{{ asset('images/japan.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="text">Japan</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Odio est tellus in turpis ligula rhoncus. Quam vitae suspendisse orci viverra eleifend semper. Viverra morbi sed enim eget aliquet. Eu sed lobortis lorem vulputate felis arcu felis fermentum. Eget massa praesent sed lorem gravida mattis auctor pretium nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
                <div class="item">
                    <div class="row">
                        <div class="col-md-4">
                            <div class="pos-rel">
                                <div class="img">
                                    <img src="{{ asset('images/korea.jpg') }}" alt="" title=""/>
                                </div>
                                <div class="text">Korea</div>
                            </div>
                        </div>
                        <div class="col-md-8">
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. Hendrerit quis leo volutpat mauris sed feugiat. Maecenas vitae consectetur aliquam eleifend blandit lectus. Scelerisque semper malesuada sit scelerisque facilisi quam. Nulla pharetra sodales congue posuere fermentum, lectus posuere in.</p>
                                <p>Odio est tellus in turpis ligula rhoncus. Quam vitae suspendisse orci viverra eleifend semper. Viverra morbi sed enim eget aliquet. Eu sed lobortis lorem vulputate felis arcu felis fermentum. Eget massa praesent sed lorem gravida mattis auctor pretium nulla.</p>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')
<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-regulation').addClass('active');
	});
</script>
@endsection