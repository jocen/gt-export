@extends('layout')

@section('content')
    <div class="pad-content pt30">
        <div class="css-account">
            <div class="container">
                <div class="row">
                    <div class="col-md-3">
                        <div class="left-account">
                            @include('member.menu')
                        </div>
                    </div>
                    <div class="col-md-9">
                        <div class="right-account">
                            <div class="title mb10">Shipment Documentation</div>
                            <div class="row">
                                <div class="col-md-10 my-auto">
                                    <div class="t2">View and download all shipping related documents. Documents can only be retrieved after payment confirmation.</div>
                                </div>
                                <div class="col-md-2 my-auto text-right resp-text-right">
                                    <div class="link">
                                        <a class="click-view">View All</a>
                                    </div>
                                </div>
                            </div>
                            <div class="bdr"></div>
                            <div class="table-order-account">
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div>
                                    <div class="tbl tbl-bdy">
                                        <div class="cell w130">02-01-2020</div>
                                        <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                        <div class="cell w110">100MB</div>
                                        <div class="cell w90">
                                            <ul class="link">
                                                <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                            </ul>
                                        </div>
                                    </div>
                                </div>
                                <div class="box-hide">
                                    <div>
                                        <div class="tbl tbl-bdy">
                                            <div class="cell w130">02-01-2020</div>
                                            <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                            <div class="cell w110">100MB</div>
                                            <div class="cell w90">
                                                <ul class="link">
                                                    <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="tbl tbl-bdy">
                                            <div class="cell w130">02-01-2020</div>
                                            <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                            <div class="cell w110">100MB</div>
                                            <div class="cell w90">
                                                <ul class="link">
                                                    <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="tbl tbl-bdy">
                                            <div class="cell w130">02-01-2020</div>
                                            <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                            <div class="cell w110">100MB</div>
                                            <div class="cell w90">
                                                <ul class="link">
                                                    <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="tbl tbl-bdy">
                                            <div class="cell w130">02-01-2020</div>
                                            <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                            <div class="cell w110">100MB</div>
                                            <div class="cell w90">
                                                <ul class="link">
                                                    <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                    <div>
                                        <div class="tbl tbl-bdy">
                                            <div class="cell w130">02-01-2020</div>
                                            <div class="cell"><span class="bold">BMW 3 Series Quotation Document.pdf</span></div>
                                            <div class="cell w110">100MB</div>
                                            <div class="cell w90">
                                                <ul class="link">
                                                    <li><a href="#"><img src="{{ asset('images/view.png') }}" alt="" title=""/></a></li>
                                                    <li><a href="#"><img src="{{ asset('images/download.png') }}" alt="" title=""/></a></li>
                                                </ul>
                                            </div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>

@endsection

@section('js')

<script type="text/javascript">
	$(document).ready(function() {
		$('.nav-ship').addClass('active');

        $('.click-view').click(function(event) {
            $(this).hide();
            $('.box-hide').show();
        });

        ('header').addClass('account');
	});
</script>
@endsection