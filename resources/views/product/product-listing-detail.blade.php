@extends('layout')

@section('content')
	
<div class="pad-content pb0">
    <div class="css-detail">
        <div class="container">
            <div class="mb70">
                <div class="row">
                    <div class="col-md-6">
                        <div class="pr40">
                            <div class="pos-rel">
                                <div class="pagingInfo"></div>
                                <div class="slider-product">
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">>
                                            <img src="{{ asset('images/detail2.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail3.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail4.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail5.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">>
                                            <img src="{{ asset('images/detail2.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail3.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail4.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail5.jpg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                    <div class="item">
                                        <a class="click-popup">
                                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                                        </a>
                                    </div>
                                </div>
                            </div>
                            <div class="video-product">
                                <iframe src="https://www.youtube.com/embed/fIMyGAs7bRc?controls=1&amp;mute=0&amp;enablejsapi=1&amp;rel=0" frameborder="0" allow="accelerometer; autoplay; clipboard-write; encrypted-media; gyroscope; picture-in-picture" allowfullscreen></iframe>
                            </div>
                            <div>
                                <ul class="clearfix slider-thumb">
                                    <li class="item" data-slick="1">
                                        <img src="{{ asset('images/thumb1.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="2">
                                        <img src="{{ asset('images/thumb2.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="3">
                                        <img src="{{ asset('images/thumb3.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="4">
                                        <img src="{{ asset('images/thumb4.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="5">
                                        <img src="{{ asset('images/thumb5.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="6">
                                        <img src="{{ asset('images/thumb1.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="7">
                                        <img src="{{ asset('images/thumb1.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="8">
                                        <img src="{{ asset('images/thumb2.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="9">
                                        <img src="{{ asset('images/thumb3.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="10">
                                        <img src="{{ asset('images/thumb4.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="11">
                                        <img src="{{ asset('images/thumb5.jpg') }}" alt="" title=""/>
                                    </li>
                                    <li class="item" data-slick="12">
                                        <img src="{{ asset('images/thumb1.jpg') }}" alt="" title=""/>
                                    </li>
                                </ul>
                            </div>
                            <div class="click-show">Show thumbnails</div>
                            <div class="click-hide">Hide thumbnails</div>
                            <div class="link-upload">
                                <a href="#"><i class="fas fa-upload"></i> Download all images</a>
                            </div>
                        </div>
                    </div>
                    <div class="col-md-6">
                        <div class="merk">Porsche</div>
                        <div class="nm">Macan GTS</div>
                        <div class="buy">Buy it at</div>
                        <div class="price">$ 500,000</div>
                        <div class="add">
                            <a href="{{ URL::to('/contact-us') }}">
                                <button type="button" class="hvr-button full100">Ask for Quote</button>
                            </a>
                        </div>
                        <div class="box-desc">
                            <div class="t-desc">What we love about this car</div>
                            <div class="desc">
                                <p>Lorem ipsum dolor sit amet, consectetur adipiscing elit. A gravida tempor tristique dignissim velit. Ut ante ultrices lectus arcu id leo.</p>
                                <p>Eget vulputate enim tellus non facilisis congue amet nisl. Aliquam sed enim mattis interdum sollicitudin sit pulvinar urna.</p>
                            </div>
                        </div>
                        <div class="bdr"></div>
                        <div class="t1">Car Details</div>
                        <div class="t2">Economy & Performance</div>
                        <ul class="l-detail">
                            <li>
                                <div class="l-detail-t1">100,000 km</div>
                                <div class="l-detail-t2">Mileage</div>
                            </li>
                            <li>
                                <div class="l-detail-t1">2014</div>
                                <div class="l-detail-t2">Year</div>
                            </li>
                            <li>
                                <div class="l-detail-t1">2,000cc</div>
                                <div class="l-detail-t2">Engine</div>
                            </li>
                            <li>
                                <div class="l-detail-t1">Automatic</div>
                                <div class="l-detail-t2">Transmission</div>
                            </li>
                            <li>
                                <div class="l-detail-t1">Petrol</div>
                                <div class="l-detail-t2">Fue</div>
                            </li>
                        </ul>
                        <div class="text1">
                            <div class="text1-row">
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Brand Brand Brand Brand background Brand</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Brand</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Model</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Model</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Transmission</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Transmission</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Chassis No</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Chassis</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Model Code</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Model</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Product Type</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Product</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Registration Year/Month</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Year</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Manufacture Year/Month</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Year</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Engine No</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Engine</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Steering</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Steering</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Drive Type</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Drive</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Color</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Color</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Engine Code</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Engine</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Number of Doors</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Number</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Seats</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Seats</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Total Seats</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Total</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Weight(kg)</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Weight</div>
                                        </div>
                                    </div>
                                </div>
                                <div class="text1-col">
                                    <div class="text1-bdr">
                                        <div class="row">
                                            <div class="col-5 col-xl-4 my-auto left">Total weight(kg)</div>
                                            <div class="col-7 col-xl-8 my-auto right">Name Total</div>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="bdr"></div>
                        <div class="t2">Exterior Features</div>
                        <div class="text2">
                            <div class="text2-row">
                                <div class="text2-col"><div class="text2-bdr active">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr active">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr active">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr active">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                                <div class="text2-col"><div class="text2-bdr">360 Degree Camera</div></div>
                            </div>
                        </div>
                        <div class="bdr"></div>
                        <div class="t2">Technical</div>
                        <div class="text3">Lorem ipsum dolor sit amet, consectetur adipiscing elit. A gravida tempor tristique dignissim velit. Ut ante ultrices lectus arcu id leo.</div>
                        <div class="bdr"></div>
                    </div>
                </div>
            </div>
        </div>
        <div class="banner" style="background: url('images/banner-detail.jpg') no-repeat center;">
            <div class="container">
                <div class="t-banner">Interested?</div>
                <div class="bdy-banner">
                    <p>Get in touch with our sales partner for more information.</p>
                </div>
                <div class="link">
                    <a href="{{ URL::to('/contact-us') }}">
                        <button type="button" class="hvr-button">Get Quote</button>
                    </a>
                </div>
            </div>
        </div>
    </div>
</div>

<div class="popup-product">
    <div class="popup-close"><i class="fas fa-times"></i></div>
    <div class="pagingInfo"></div>
    <div class="abs-popup">
        <div class="row justify-content-center">
            <div class="col-lg-8 col-xl-7">
                <div class="slider-popup-product">
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail2.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail3.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail4.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail5.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail2.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail3.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail4.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail5.jpg') }}" alt="" title=""/>
                        </a>
                    </div>
                    <div class="item">
                        <a>
                            <img src="{{ asset('images/detail.jpeg') }}" alt="" title=""/>
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>

@endsection

@section('js')
<script type="text/javascript" src="{{ asset('js/jquery.zoom.min.js') }}"></script>
<!-- <script type="text/javascript" src="{{ asset('js/fancybox/jquery.fancybox.min.js') }}"></script> -->

<script type="text/javascript">
	$(document).ready(function() {
        var $status = $('.pagingInfo');

        $('.slider-product').on('init reInit afterChange', function(event, slick, currentSlide, nextSlide) {
            var i = (currentSlide ? currentSlide : 0) + 1;
            $status.text(i + '/' + slick.slideCount);
        });

        $('.slider-product').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            fade: false,
            asNavFor: '.slider-thumb,.slider-popup-product',
            infinite: false
        });

        $('.slider-thumb').slick({
            slidesToShow: 5,
            slidesToScroll: 1,
            asNavFor: '.slider-product,.slider-popup-product',
            dots: false,
            arrows: false,
            centerMode: false,
            focusOnSelect: true,
            infinite: false,
            responsive: [
                {
                    breakpoint: 768,
                    settings: {
                        slidesToShow: 4,
                    }
                }
            ]
        });

        $('.click-show').click(function() {
            $('.slider-thumb').slick('unslick');
            $(this).hide();
            $('.click-hide').show();

            $('.slider-thumb .item').click(function() {
                var getSlick = $(this).attr('data-slick');
                $('.slider-product').slick('slickGoTo', getSlick-1);
            });
        });

        $('.slider-popup-product').slick({
            slidesToShow: 1,
            slidesToScroll: 1,
            arrows: true,
            dots: false,
            swipe: false,
            fade: false,
            asNavFor: '.slider-product,.slider-thumb',
            infinite: false
        });

        $('.click-popup').click(function(event) {
            $('.popup-product').show();
            $('body').addClass('no-scroll-popup');
            $('.slider-popup-product').slick('setPosition');
        });

        //$('.slider-popup-product').slick('unslick');

        $('.popup-close').click(function(event) {
            $('.popup-product').hide();
            $('body').removeClass('no-scroll-popup');
            // $('.slider-popup-product').slick('unslick');
        });

        $('.slider-popup-product .item').zoom({ on:'grab' });

        $('.click-hide').click(function() {
            $('.slider-thumb').slick({
                slidesToShow: 5,
                slidesToScroll: 1,
                asNavFor: '.slider-product',
                dots: false,
                arrows: false,
                centerMode: false,
                focusOnSelect: true,
                infinite: false,
                responsive: [
                    {
                        breakpoint: 768,
                        settings: {
                            slidesToShow: 4,
                        }
                    }
                ]
            });
            $(this).hide();
            $('.click-show').show();
        });

        $('.nav-product').addClass('active');

        // $("[data-fancybox]").fancybox({
        //     buttons : [
        //         'close',
        //     ],
        //     loop: false,
        //     idleTime: false,
        // });
	});
</script>
@endsection